/*
 *                                                                            
 *  Copyright (c) 2011 - 2016 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
 package org.eclipse.osbp.abstractstatemachine;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.ClassUtils;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.ui.api.statemachine.IStateMachine;
import org.eclipse.osbp.ui.api.statemachine.IStateMachineParticipant;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractStateMachineParticipant implements
		IStateMachineParticipant {
	/** The LOGGER. */
	protected static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractStateMachineParticipant.class);
	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	protected IStateMachine statemachine;
	protected Locale locale;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName,
			PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(String propertyName,
			PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(propertyName, listener);
	}

	@Override
	public IStateMachine getStatemachine() {
		return statemachine;
	}

	@Override
	public void setStatemachine(IStateMachine statemachine) {
		this.statemachine = statemachine;
	}
	
	@Override
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	@Override
	public Method getter(Object listener, String id) {
		if (id == null) {
			return null;
		}
		Class<?> clz = listener.getClass();
		try {
			BeanInfo info = Introspector.getBeanInfo(clz);
			for (PropertyDescriptor desc : info.getPropertyDescriptors()) {
				if (desc.getName().equalsIgnoreCase(id)) {
					return desc.getReadMethod();
				}
			}
		} catch (IntrospectionException e) {
			LOGGER.error("getter(x,y) exception: ", e);
		}
		return null;
	}

	@Override
	public Method getter(Object listener, String id, String device) {
		if (id == null) {
			return null;
		}
		Class<?> clz = listener.getClass();
		try {
			BeanInfo info = Introspector.getBeanInfo(clz);
			String name = "get"+id;
			for (MethodDescriptor desc : info.getMethodDescriptors()) {
				if (desc.getName().equalsIgnoreCase(name)) {
					Method method = desc.getMethod();
					Class<?>[] paras = method.getParameterTypes();
					if(paras.length == 1) {
						return desc.getMethod();
					}
				}
			}
		} catch (IntrospectionException e) {
			LOGGER.error("getter(x,y,z) exception: ", e);
		}
		return null;
	}
	
	@Override
	public Method setter(Object listener, String id, Class<?> parameter, int contentIndex) {
		if (id == null) {
			return null;
		}
		Class<?> clz = listener.getClass();
		try {
			BeanInfo info = Introspector.getBeanInfo(clz);
			for (MethodDescriptor desc : info.getMethodDescriptors()) {
				if (desc.getName().equalsIgnoreCase("set"+id)) {
					Method method = desc.getMethod();
					Class<?>[] paras = method.getParameterTypes();
					if(paras.length > contentIndex && (ClassUtils.isAssignable(paras[contentIndex], parameter, true) || ClassUtils.isAssignable(parameter, paras[contentIndex], true))) {
						return desc.getMethod();
					}
				}
			}
		} catch (IntrospectionException e) {
			LOGGER.error("setter(x,y,z) exception: ", e);
		}
		return null;
	}

	@Override
	public Method setter(Object listener, String id, Class<?> parameter) {
		return setter(listener, id, parameter, 0);
	}
	@Override 
	public Method setter(Object listener, String id, Object content, int contentIndex) {
		if(content != null) {
			return setter(listener, id, content.getClass(), contentIndex);
		} else {
			return setter(listener, id, null, contentIndex);
		}
	}

	@Override
	public Class<?> getReturnType(Object obj, String id) {
		if (id == null) {
			return null;
		}
		Class<?> clz = obj.getClass();

		try {
			BeanInfo info = Introspector.getBeanInfo(clz);
			for (PropertyDescriptor desc : info.getPropertyDescriptors()) {
				if (desc.getName().equalsIgnoreCase(id)) {
					Method method = desc.getReadMethod();
					return method.getReturnType();
				}
			}
		} catch (IntrospectionException e) {
			LOGGER.error("getReturnType exception: ", e);
		}
		return null;
	}

	@Override
	public void toggle(Object obj, String id) {
		Object result = get(obj, id);
		if(result != null) {
			if(result.getClass().isAssignableFrom(Boolean.class)) {
				set(obj, id, result);
			}
			if(result.getClass().isAssignableFrom(boolean.class)) {
				set(obj, id, (boolean)result);
			}
		} else {
			set(obj, id, true);
		}
	}
	
	@Override
	public void set(Object obj, String id, Object content) {
		try {
			Method method = setter(obj, id, content, 0);
			if (method != null) {
				method.invoke(obj, content);
			}
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			LOGGER.error("set(x,y,z) exception: ", e);
		}
	}

	@Override
	public void set(Object obj, String id, String device, Object content) {
		try {
			Method method = setter(obj, id, content, 1);
			if (method != null) {
				method.invoke(obj, device, content);
			}
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			LOGGER.error("set(w,x,y,z) exception: ", e);
		}
	}
	
	@Override
	public Object get(Object obj, String id) {
		Method method = getter(obj, id);
		if (method == null) {
			return null;
		}
		try {
			return method.invoke(obj);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			LOGGER.error("get(x,y) exception: ", e);
			return null;
		}
	}

	@Override
	public Object get(Object obj, String id, String device) {
		Method method = getter(obj, id, device);
		if (method == null) {
			return null;
		}
		try {
			return method.invoke(obj, device);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			LOGGER.error("get(x,y,z) exception: ", e);
			return null;
		}
	}
	
	@Override
	public void clear(Object obj, String id) {
		Method method = setter(obj, id, "", 0);
		if (method != null) {
			try {
				method.invoke(obj, "");
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
		method = setter(obj, id, 0.0, 0);
		if (method != null) {
			try {
				method.invoke(obj, 0.0);
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
		method = setter(obj, id, 0, 0);
		if (method != null) {
			try {
				method.invoke(obj, 0);
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
		method = setter(obj, id, 0L, 0);
		if (method != null) {
			try {
				method.invoke(obj, 0L);
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
		method = setter(obj, id, IDto.class, 0);
		if (method != null) {
			try {
				method.invoke(obj, (Object[])null);
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
		method = setter(obj, id, Date.class, 0);
		if (method != null) {
			try {
				method.invoke(obj, (Date)null);
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
		method = setter(obj, id, DateTime.class, 0);
		if (method != null) {
			try {
				method.invoke(obj, (DateTime)null);
				return;
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				LOGGER.error("clear exception: ", e);
			}
		}
	}

}
