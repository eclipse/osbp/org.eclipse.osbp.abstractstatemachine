/*
 *                                                                            
 *  Copyright (c) 2011 - 2016 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
package org.eclipse.osbp.abstractstatemachine;

import org.eclipse.osbp.runtime.common.session.ISessionManager;
import org.eclipse.osbp.ui.api.pos.IPOSService;
import org.eclipse.osbp.ui.api.pos.ISignatureService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class POSServiceBinder {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger("servicebinder");

	private static IPOSService posService;
	private static ISessionManager sessionManager;
	private static ISignatureService signatureService;

	@Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.DYNAMIC)
	public void bindPOSService(IPOSService posService) {
		LOGGER.debug("FSM POSService bound");
		POSServiceBinder.posService = posService;
	}

	public void unbindPOSService(IPOSService posService) {
		LOGGER.debug("FSM POSService unbound");
		POSServiceBinder.posService = null;
	}

	public static IPOSService getPosService() {
		return posService;
	}

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
	public synchronized void bindSessionManager(final ISessionManager sessionManager) {
		POSServiceBinder.sessionManager = sessionManager;
		LOGGER.debug("FSM SessionManager bound");
	}

	public synchronized void unbindSessionManager(final ISessionManager sessionManager) {
		POSServiceBinder.sessionManager = null;
		LOGGER.debug("FSM SessionManager unbound");
	}

	public static ISessionManager getSessionManager() {
		return sessionManager;
	}

	@Reference(cardinality = ReferenceCardinality.OPTIONAL, policy = ReferencePolicy.STATIC)
	public synchronized void bindSignatureService(final ISignatureService signatureService) {
		POSServiceBinder.signatureService = signatureService;
		LOGGER.debug("FSM SignatureService bound");
	}
	
	public synchronized void unbindSignatureService(final ISignatureService signatureService) {
		POSServiceBinder.signatureService = null;
		LOGGER.debug("FSM SignatureService unbound");
	}
	
	public static ISignatureService getSignatureService() {
		return signatureService;
	}
}
