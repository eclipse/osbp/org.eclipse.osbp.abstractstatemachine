/*
 *                                                                            
 *  Copyright (c) 2011 - 2016 - Loetz GmbH & Co KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG
 * 
 */
package org.eclipse.osbp.abstractstatemachine;

import java.util.TimerTask;

import org.eclipse.osbp.ui.api.message.MessageEvent;
import org.eclipse.osbp.ui.api.statemachine.IStateMachine;

import com.vaadin.ui.UI;

public class TaskEventSource extends TimerTask {
	private IStateMachine statemachine;
	private MessageEvent event;

	public TaskEventSource(IStateMachine statemachine, MessageEvent event) {
		this.statemachine = statemachine;
		this.event = event;
	}

	@Override
	public void run() {
		if (UI.getCurrent().isAttached())
			statemachine.processEvent(statemachine, event);
	}

}
